# KDE Akademy Group Photo Automator

Automatically mark up faces on Akademy Group photos

Clone Git face recognition magic
    git clone https://github.com/ageitgey/face_recognition.git

Build docker image for it
    cd face_recognition
    docker build -t face .
    cd ..

Run magic
    docker run -v `pwd`:/build -ti face bash -c "cd /build; ./face-box.py akademy2017-groupphoto.jpg" > index.html

