#!/usr/bin/env python3

import face_recognition as fr
import sys
from PIL import Image, ImageDraw

try:
    _, img_name = sys.argv
except:
    raise SystemExit("usage: " + sys.argv[0] + " image")

img = fr.load_image_file(img_name)
faces = fr.face_locations(img)

pil = Image.fromarray(img)
pil = pil.convert("RGBA")

tmp = Image.new('RGBA', pil.size, (0,0,0,0))
draw = ImageDraw.Draw(tmp)

print('''
<?xml version="1.0" encoding="utf-8" ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
              "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en" xml:lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
<title>Akademy 2017 Group Photo</title>
<link href="../../photosearch.css" rel="stylesheet" type="text/css"/>
</head>
<body>
<p style='z-index:10;background:white;opacity:0.8;filter:opacity(80);position:fixed;width:16em;top:0px;right:0px;border-color:grey;border-style: solid;padding-left: 1em'><span id='hitcount'>Type a name to find a person:</span><input style='opacity:1;filter:opacity(100);' type='text' id="input"/>
<br/>You can help by adding names of people you recognise to the <a href="https://notes.kde.org/p/akademy-2017-group-photo">list</a> which will then be used periodically to update this page
<br /><br /><a href="http://creativecommons.org/licenses/by/4.0/"><img src="/~duffus/images/ccby.png" alt="CC BY" /></a> Photo by <a href="mailto:guillefuertes@gmail.com">Guille Fuertes</a></p>

<img src="akademy2017-groupphoto.jpg"  usemap="#unnamed"/>
<map name="unnamed" id="unnamed">
''')

count = 0
for (y0, x1, y1, x0) in faces:
    count = count + 1
    draw.rectangle(((x0,y0), (x1, y1)), fill=(30, 0, 0, 200))
    print('<area shape="rect" coords="'+str(x0)+','+str(y0)+','+str(x1)+','+str(y1)+'" onmouseover="setStatus(this.title)" title="XXX '+str(count)+'" />')

print('''
</map>
<div id='overlay' style='position:absolute;border:1px solid green;top:0px;left:0px;' ></div>
</body>
<script src="../../photosearch.js"></script>
</html>
''')

del draw
pil = Image.alpha_composite(pil, tmp)
pil = pil.convert("RGB")

img_name = img_name.replace(".", "-box.")
pil.save(img_name)
